# This is a test Merge Request template
Created from branch: %{source_branch}

Will be merged to: %{target_branch}

Created by: %{co_authored_by}


Preview link -> (not yet available)


<details>
  <summary>Commit list</summary>
  List of all pushed commits:
  %{all_commits}
</details>
